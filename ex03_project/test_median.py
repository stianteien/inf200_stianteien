# -*- coding: utf-8 -*-

__author__ = 'Stian Teien'
__email__ = 'steien@nmbu.no'


import pytest


def median(data):
    """
    Returns median of data.

    :param data: An iterable of containing numbers
    :return: Median of data
    """

    sdata = sorted(data)
    n = len(sdata)
    if(n>0):
        return (sdata[n//2] if n % 2 == 1
                else 0.5 * (sdata[n//2 - 1] + sdata[n//2]))



def test_one_element_list():
    list = [1]
    assert median(list) == 1


def test_odd_numebers():
    list = [1,2,3]

def test_even_numbers():
    list = [1,2,3,4]
    median(list)

def test_ordered_numbers():
    list = [1,2,3,4,5,6,7]
    median(list)

def test_reversed_numbers():
    list = [7,6,5,4,3,2,1]
    median(list)

def test_unordered_numbers():
    list = [7,5,6,4,2,3,1]
    median(list)

def test_empty_list():
    list = []
    median(list) == []

def test_org_data_unchanged():
    list = [1,2,3,7,5,4,3]
    temp = list
    median(list)
    assert temp == list

def test_works_with_tuples():
    list = ("apple", "banana", "cherry")
    median(list)

def test_works_with_list():
    list = ["apple", "banana", "cherry"]
    median(list)
