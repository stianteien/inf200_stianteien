# -*- coding: utf-8 -*-

__author__ = 'Stian Teien'
__email__ = 'steien@nmbu.no'


import pytest


def bubble_sort(liste):
    new_liste = [x for x in liste]
    for _ in range(len(new_liste)):
        for j in range(len(new_liste)-1):
            if(new_liste[j] > new_liste[j+1]):
                temp = new_liste[j]
                new_liste[j] = new_liste[j+1]
                new_liste[j+1] = temp

    return new_liste


def test_empty():
    """Test that the sorting function works for empty list"""
    list = []
    assert bubble_sort(list) == []

def test_single():
    """Test that the sorting function works for single-element list"""
    list = [1]
    assert bubble_sort(list) == [1]

def test_sorted_is_not_original():
    """
    Test that the sorting function returns a new object.

    Consider

    data = [3, 2, 1]
    sorted_data = bubble_sort(data)

    Now sorted_data shall be a different object than data,
    not just another name for the same object.
    """
    data = [3,2,1]
    sorted_data = bubble_sort(data)
    print(id(data), " ", id(sorted_data))
    assert (id(data) is not id(sorted_data))


def test_original_unchanged():
    """
    Test that sorting leaves the original data unchanged.

    Consider

    data = [3, 2, 1]
    sorted_data = bubble_sort(data)

    Now data shall still contain [3, 2, 1].
    """
    data = [3,2,1]
    sorted_data = bubble_sort(data)
    assert data is data



def test_sort_sorted():
    """Test that sorting works on sorted data."""
    data = [1,2,3,4,5,6]
    assert bubble_sort(data) == [1,2,3,4,5,6]


def test_sort_reversed():
    """Test that sorting works on reverse-sorted data."""
    data = [7,6,5,4,3,2,1]
    assert bubble_sort(data) == [1,2,3,4,5,6,7]


def test_sort_all_equal():
    """Test that sorting handles data with identical elements."""
    data = [2,2,2,2,2]
    assert bubble_sort(data) == [2,2,2,2,2]

def test_sorting():
    """
    Test sorting for various test cases.

    This test case should test sorting of a range of data sets and
    ensure that they are sorted correctly. These could be lists of
    numbers of different length or lists of strings.
    """
    data = [(1,2,3,4,5,),
            (1,7,6,4,3,6,4,3),
            ("b","k","abrakadabra","hei","b")]
    for i in data:
        bubble_sort(i)



