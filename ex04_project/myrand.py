# -*- coding: utf-8 -*-

__author__ = 'Stian Teien'
__email__ = 'steien@nmbu.no'




class LGRand:
    def __init__(self):
        self.a = 7 ** 5
        self.m = 2 ** 31 - 1


    def rand(self, the_seed):

        self.r = [0]*the_seed
        self.r.append(0)
        self.r[0] = self.a % self.m
        for self.n in range(the_seed):
            self.r[self.n + 1] = self.a * self.r[self.n] % self.m

        return self.r



class ListRand:
    def __init__(self, listen):
        self.liste = listen
        self.times_called_for = -1

    def rand(self):
        try:
            self.times_called_for += 1
            return self.liste[self.times_called_for]

        except RuntimeError:
            print(RuntimeError)


the_seed = 10

r1 = LGRand()
print(r1.rand(the_seed))
r2 = ListRand(r1.rand(the_seed))

for i in range(the_seed+20):
    print(r2.rand())