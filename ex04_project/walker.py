# -*- coding: utf-8 -*-

__author__ = 'Stian Teien'
__email__ = 'steien@nmbu.no'


import random

class Walker:
    def __init__(self, pos_now, pos_home):
        self.x0 = pos_now
        self.h = pos_home


    def is_at_home(self):
        if(self.position == self.h):
            return True
        else:
            return False


    def get_position(self):
        print(self.position)


    def get_steps(self):
        print(self.steps)


    def start_walk(self):
        self.position = self.x0
        self.steps = 0
        while(not self.is_at_home()):
            if (random.uniform(0, 2) >= 1):
                self.position += 1
                self.steps += 1
            else:
                self.position -= 1
                self.steps += 1

        return self.steps






avstander = [1, 2, 5, 10, 20, 50, 100]

for i in avstander:
    antall_steg = []
    student = Walker(0, i)
    for j in range(5):
         antall_steg.append(student.start_walk())

    print("Distance:\t",i," -> Path lengths: ",sorted(antall_steg))






