from random import randint

__author__ = 'Stian Teien'
__email__ = 'steien@nmbu.no'


def gjettet():
    guessinput = 0
    while guessinput < 1:
        guessinput = int(input('Your guess: '))
    return guessinput

def tilfeldig_tall():
    return randint(1, 6) + randint(1, 6)


def check(riktig_tall, guess):
    return riktig_tall == guess

if __name__ == '__main__':

    checkifcorrect = False
    forsøk = 3
    riktig_tall = tilfeldig_tall()
    while not checkifcorrect and forsøk > 0:
        guess = gjettet()
        checkifcorrect = check(riktig_tall, guess)
        if not checkifcorrect:
            print('Wrong, try again!')
            forsøk -= 1

    if forsøk > 0:
        print('You won {} points.'.format(forsøk))
    else:
        print('You lost. Correct answer: {}.'.format(riktig_tall))