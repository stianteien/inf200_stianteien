# -*- coding: utf-8 -*-

__author__ = 'Stian Teien'
__email__ = 'steien@nmbu.no'



def letter_freq(txt):
    ordbok = {}
    ord = list(txt)
    for i in ord:
        if(i in ordbok):
            ordbok[i] += 1
        else:
            ordbok[i] = 1
    return ordbok


if __name__ == '__main__':
    text = input('Please enter text to analyse: ')

    frequencies = letter_freq(text)
    for letter, count in sorted(frequencies.items()):
        print('{:3}{:10}'.format(letter, count))