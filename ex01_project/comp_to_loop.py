# -*- coding: utf-8 -*-

__author__ = 'Stian Teien'
__email__ = 'steien@nmbu.no'



def squares_by_loop(n):
    square = []
    for k in range(n):
        if k % 3 == 1:
            square.append(k**2)
    return square


def squares_by_comp(n):
    return [k**2 for k in range(n) if k % 3 == 1]


if __name__ == '__main__':
    n = 10
    if squares_by_loop(n) != squares_by_comp(n):
        print('ERROR!')